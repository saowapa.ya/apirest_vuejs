﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Web.Http;
using WebAPIRESTful.Models;

namespace WebAPIRESTful.Controllers
{
    public class RestaurantController : ApiController
    {
        public static string key = "Restaurant";
        public static MemoryCache memoryCache = new MemoryCache(key);
        public CacheItemPolicy policy = new CacheItemPolicy { AbsoluteExpiration = DateTimeOffset.UtcNow.AddMinutes(50000) };

        // GET: api/Restaurant
        public Object Get()
        {
            var Cache = GetCache(key);
            if (Cache == null)
            {
                Cache = LoadData();
                AddCache(key, Cache);
                UpdateCache(key, null);
            }
            //string json = JsonConvert.SerializeObject(Cache);
            return Cache;
        }

        public void AddCache(string key, object data)
        {
            memoryCache.Add(key, data, policy);
        }

        public void UpdateCache(string key, object input)
        {
            // new item 
            Restaurant Restaurants = new Restaurant { Id = 5, Name = "ข้าวหมูแดงอบน้ำผึ้ง-เตาปูน", Description = "569/3 ถนน กรุงเทพ - นนทบุรี แขวง บางซื่อ เขตบางซื่อ กรุงเทพมหานคร 10800", Lat = 13.8107046, Lng = 100.5328517, position = new Position() { Lat = 13.8107046, Lng = 100.5328517 } };

            var cache = GetCache(key);
            if (cache != null)
            {
                var data = (List<Restaurant>)cache;
                var result = data.Where(w => w.Name.Equals(Restaurants.Name));
                if (result.Any())
                {
                    // no update
                }
                else
                {
                    // update
                    data.Add(Restaurants);
                    memoryCache.Set(key, data, policy);
                }
            }
        }

        public object GetCache(string key)
        {
            object Cache = memoryCache.Get(key);
            return Cache;
        }

        public List<Restaurant> LoadData()
        {
            List<Restaurant> Restaurants = new List<Restaurant>();
            Restaurants.Add(new Restaurant { Id = 1, Name = "บะหมี่ปราบเซียน ประชาชื่น 8", Title = "บะหมี่ปราบเซียน ประชาชื่น 8", Description = "293 27 ซอย ประชาชื่น 8 แขวง บางซื่อ เขตบางซื่อ กรุงเทพมหานคร 10800",  Lat = 13.8069079, Lng = 100.541996, position = new Position() { Lat = 13.8069079, Lng = 100.541996 } } );
            Restaurants.Add(new Restaurant { Id = 2, Name = "ก๋วยเตี๋ยวเตาปูน(ล็อคล้อ)", Title = "ก๋วยเตี๋ยวเตาปูน(ล็อคล้อ)", Description = "10, 6 ถนน ประชาชื่น แขวง บางซื่อ เขตบางซื่อ กรุงเทพมหานคร 10800", Lat = 13.8142846, Lng = 100.5432543, position = new Position() { Lat = 13.8142846, Lng = 100.5432543 } } );
            Restaurants.Add(new Restaurant { Id = 3, Name = "อร่อยแล้วสั่งต่อ", Title = "อร่อยแล้วสั่งต่อ", Description = "812/10 ซอย ประชาชื่น 25 ถนน ประชาชื่น แขวง วงศ์สว่าง เขตบางซื่อ กรุงเทพมหานคร 10800", Lat = 13.8149514, Lng = 100.5259594, position = new Position() { Lat = 13.8149514, Lng = 100.5259594 } } );
            Restaurants.Add(new Restaurant { Id = 4, Name = "คำโฮม คาเฟ่", Title = "คำโฮม คาเฟ่", Description = "คอนโดยูดีไลท์ 695/4 ซอย ประชาชื่น 19 แขวง บางซื่อ เขตบางซื่อ กรุงเทพมหานคร 10800", Lat = 13.8184855, Lng = 100.5359784, position = new Position() { Lat = 13.8184855, Lng = 100.5359784 } } );

            return Restaurants;
        }

        // GET: api/Restaurant/5
        public Object Get(string id)
        {
            var CacheObj = Get();
            List<Restaurant> Restaurants = CacheObj as List<Restaurant>;

            var res = Restaurants.Where(e => e.Name.Contains(id));

            return res;
        }


    }
}